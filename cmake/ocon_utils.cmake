find_package(Protobuf 3.6.1 REQUIRED CONFIG)
find_package(gRPC 1.15.1 REQUIRED CONFIG)

set(GRPC_GRPCPP gRPC::grpc++)
set(GRPC_CPP_PLUGIN_EXECUTABLE $<TARGET_FILE:gRPC::grpc_cpp_plugin>)
set(PROTOBUF_PROTOC $<TARGET_FILE:protobuf::protoc>)

function(ocon_protobuf_generate_cpp PROTO_SRCS_NAME PROTO_HDRS_NAME [PROTO_FILES])
    set(GRPC_CPP_PLUGIN ${GRPC_CPP_PLUGIN_EXECUTABLE})
    set(PROTO_SRCS)
    set(PROTO_HDRS)
    foreach(PROTO_FILE ${PROTO_FILES})
        get_filename_component(PROTO ${PROTO_FILE} ABSOLUTE)
        get_filename_component(PROTO_PATH ${PROTO} PATH)
        get_filename_component(PROTO_ABS_PATH ${PROTO_PATH} ABSOLUTE)
        get_filename_component(PROTO_NAME_WE ${PROTO_FILE} NAME_WE)
        set(PROTO_SRC "${CMAKE_CURRENT_BINARY_DIR}/${PROTO_NAME_WE}.pb.cc")
        set(PROTO_HDR "${CMAKE_CURRENT_BINARY_DIR}/${PROTO_NAME_WE}.pb.h")
        set(GRPC_SRC "${CMAKE_CURRENT_BINARY_DIR}/${PROTO_NAME_WE}.grpc.pb.cc")
        set(GRPC_HDR "${CMAKE_CURRENT_BINARY_DIR}/${PROTO_NAME_WE}.grpc.pb.h")
        list(APPEND PROTO_SRCS "${PROTO_SRC}" "${GRPC_SRC}")
        list(APPEND PROTO_HDRS "${PROTO_HDR}" "${GRPC_HDR}")
        add_custom_command(
            OUTPUT "${PROTO_SRC}" "${PROTO_HDR}" "${GRPC_SRC}" "${GRPC_HDR}"
            COMMAND ${PROTOBUF_PROTOC}
            ARGS
                --cpp_out "${CMAKE_CURRENT_BINARY_DIR}"
                --grpc_out "${CMAKE_CURRENT_BINARY_DIR}"
                --proto_path "${PROTO_ABS_PATH}"
                --plugin=protoc-gen-grpc=${GRPC_CPP_PLUGIN}
                ${PROTO}
            DEPENDS ${PROTO}
        )
    endforeach()
    set(${PROTO_SRCS_NAME} ${PROTO_SRCS} PARENT_SCOPE)
    set(${PROTO_HDRS_NAME} ${PROTO_HDRS} PARENT_SCOPE)
endfunction()