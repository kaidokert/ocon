/*
 *
 * Copyright 2018 Peter de Lange.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include <pthread.h>
#include <math.h>
#include <google/protobuf/empty.pb.h>
#include <google/protobuf/util/json_util.h>

#include <ocon/server.h>
#include <ocon/node.h>

#include "server_websocket.h"
#include "server_grpc.h"

namespace ocon {


Server::Server(Node* root_node, ServerConfiguration config)
      : config_(std::move(config)), root_node_(root_node)
{
    // check the config
    if (config_.host().empty()) config_.set_host("127.0.0.1");
    if (config_.port_grpc() == 0) config_.set_port_grpc(50051);
    if (config_.port_websocket() == 0) config_.set_port_websocket(8080);

    // create websocket and grpc server objects
    server_ws_ = std::make_unique<ServerWebSocket>(std::bind(&Server::WebSocketStreamData,
            this, std::placeholders::_1, std::placeholders::_2), config_);
    server_grpc_ = std::make_unique<ServerGRPC>(config_);

    // Register server grpc service
    server_grpc_->AddService(this);

    // Register node grpc services
    for (auto& service : *root_node_->grpc_services()) {
        server_grpc_->AddService(service);
    }

    server_info_.set_status(ServerStatusTypes::IDLE);
}

Server::~Server()
{
    // stop the real time controller thread
    if (thread_running_) {
        thread_running_ = false;
        pthread_join(rt_thread_, nullptr);
    }
}

const ServerConfiguration& Server::config() const {
    return config_;
}


ServerInfo Server::server_info() const {
    std::lock_guard<std::mutex> guard(mu_);
    return server_info_;
}

void Server::Run() {
    // start the real time controller thread
    std::lock_guard<std::mutex>guard(mu_);
    server_info_.set_status(ServerStatusTypes::STARTED);

    if (config_.scheduler()) {
        // real time thread priority configuration
        pthread_attr_init(&rt_thread_attrib_);
        pthread_attr_setschedpolicy(&rt_thread_attrib_, SCHED_FIFO);
        rt_thread_param_.sched_priority = 80;
        pthread_attr_setschedparam(&rt_thread_attrib_, &rt_thread_param_);
        pthread_attr_setinheritsched(&rt_thread_attrib_, PTHREAD_EXPLICIT_SCHED);
        // Start the thread
        thread_running_ = true;
        if (pthread_create(&rt_thread_, &rt_thread_attrib_, &Server::RealTimeScheduler, (void*)this) == 0) {
            server_info_.mutable_scheduler()->set_priority_thread(true);
        } else {
            pthread_create(&rt_thread_, nullptr, &Server::RealTimeScheduler, (void*)this);
        }
    }
    server_ws_->Run();
    server_grpc_->Run();
    server_info_.set_status(ServerStatusTypes::RUNNING);
}


void* Server::RealTimeScheduler(void* data) {

    auto* server = static_cast<Server*>(data);

    // Always start with execute init
    server->root_node_->ExecuteInit();

    using namespace std::chrono;

    // define execution time in seconds and as duration
    double time_step = server->config().time_step();
    auto time_step_duration = duration_cast<steady_clock::duration>(duration<double>(time_step));
    SchedulerInfo* scheduler_info = server->server_info_.mutable_scheduler();
    {
        std::lock_guard<std::mutex>guard(server->mu_);
        scheduler_info->set_execution_interval(time_step);
    }

    // start the loop
    auto next_start_time = steady_clock::now();
    while (server->thread_running_) {

        // set the next start time
        next_start_time += time_step_duration;

        // execute update
        server->root_node_->ExecuteStep(time_step);

        auto now = steady_clock::now();
        auto idle_time_nano_seconds = next_start_time - now;

        // time in seconds as double
        double idle_time = std::chrono::duration<double>(idle_time_nano_seconds).count();
        double execution_time = time_step - idle_time;

        // check for over run or sleep till next start time
        if (idle_time_nano_seconds.count() > 0) {
            {
                std::lock_guard<std::mutex> guard(server->mu_);
                scheduler_info->set_idle_time(idle_time);
                scheduler_info->set_execution_time(time_step - idle_time);
                scheduler_info->set_execution_time_max(std::max(scheduler_info->execution_time_max(), execution_time));
            }
            std::this_thread::sleep_until(next_start_time);
        } else {
            std::cout << "Overrun detected" << std::endl;
            {
                std::lock_guard<std::mutex>guard(server->mu_);
                scheduler_info->set_idle_time(idle_time);
                scheduler_info->set_execution_time(time_step - idle_time);
                scheduler_info->set_overruns(scheduler_info->overruns() + 1);
                scheduler_info->set_execution_time_max(std::max(scheduler_info->execution_time_max(), execution_time));
            }
            next_start_time = now;
        }
    }
    return nullptr;
}


bool Server::WebSocketStreamData(const std::string &request, std::string &response) {
    NodeWebStream result;
    result.mutable_node_tree()->CopyFrom(root_node_->tree());
    {
        std::lock_guard<std::mutex>guard(mu_);
        server_info_.set_open_websocket_connections(static_cast<uint32>(server_ws_->number_of_open_connections()));
        result.mutable_server()->CopyFrom(server_info_);
    }
    util::JsonPrintOptions options;
    options.preserve_proto_field_names = true;
    auto status = util::MessageToJsonString(result, &response, options);
    return status.ok();
}

grpc::Status Server::GetServerInfo(grpc::ServerContext *, const Empty *request, ServerInfo *response) {
    *response = server_info();
    return grpc::Status::OK;
}


}