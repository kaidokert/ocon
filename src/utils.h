/*
 *
 * Copyright 2018 Peter de Lange.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#ifndef OCON_UTILS_H
#define OCON_UTILS_H

#include <ocon/ocon_node.pb.h>
#include <ocon/common.h>


namespace ocon {


// Auxiliary recursive method used by search message.
// Scans a message object recursively and returns true and a set of field descriptors when the message is found.
bool RecursiveMessageSearch(const Message &message, const Message &candidate, Fields_t &fields);


// Allocates all sub messages within a message
void AllocateMessages(Message* message);


// Creates a dot separated path string given a set of fields. Returns empty in case of empty fields.
std::string CreateMessagePathFromFields(const Fields_t& fields);


// Creates a node message reference message given a node, set of fields, and interface type.
NodeMessageReference CreateNodeMessageReference(const ocon::Node *node,
        const std::string& interface_name, const Fields_t &fields);


// Auxiliary method to recursively find a message, given a set of field descriptors.
Message *MessageFieldResolver(Message *message, const std::vector<const FieldDescriptor *> &fields, unsigned index = 0);


// Recursive method to parse a message with a given path string and generate a set of field descriptors
// used for signal binding execution. returns false in case of error.
bool GenerateFieldsFromPath(const Message* message, const std::string& path,
        std::vector<const FieldDescriptor*>& fields);


} // namespace ocon

#endif //OCON_UTILS_H
