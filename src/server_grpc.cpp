/*
 *
 * Copyright 2018 Peter de Lange.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include <memory>

#include <google/protobuf/empty.pb.h>

#include <ocon/node.h>

#include "server_grpc.h"


namespace ocon {


ServerGRPC::ServerGRPC(const ServerConfiguration& config)
      : host_(config.host()), port_(static_cast<const unsigned short>(config.port_grpc()))
{
}

ServerGRPC::~ServerGRPC()
{
    // shutdown the server
    if (server_) {
        server_->Shutdown();
    }
}

void ServerGRPC::Run()
{
    // start the real time controller thread
    std::string server_address(host_ + ":" + std::to_string(port_));
    grpc::ServerBuilder builder;
    // Listen on the given address without any authentication mechanism.
    builder.AddListeningPort(server_address, grpc::InsecureServerCredentials());

    // Register "service_" as the instance through which we'll communicate with
    // clients. In this case it corresponds to an *asynchronous* service.
    for (auto& service : services_) {
        builder.RegisterService(service);
    }

    // Finally assemble the server.
    server_ = builder.BuildAndStart();

    std::cout << "OCON gRPC server listening on " << server_address << std::endl;
}

void ServerGRPC::AddService(grpc::Service *service) {
    services_.emplace_back(service);
}


} // namespace ocon
