/*
 *
 * Copyright 2018 Peter de Lange.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#ifndef OCON_SERVER_GRPC_H
#define OCON_SERVER_GRPC_H

#include <grpcpp/grpcpp.h>

#include <ocon/common.h>
#include <ocon/ocon_server.pb.h>


namespace ocon {


class ServerGRPC final {

    std::mutex mu_;

    const std::string host_;
    const unsigned short port_;

    std::vector<grpc::Service*> services_;

    // grpc server objects
    std::unique_ptr<grpc::Server> server_;

public:

    explicit ServerGRPC(const ServerConfiguration& config);
    ~ServerGRPC();

    void Run();

    void AddService(grpc::Service* service);

};

} // namespace ocon

#endif //OCON_SERVER_GRPC_H
