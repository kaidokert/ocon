/*
 *
 * Copyright 2018 Peter de Lange.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include <algorithm>
#include<fstream>


#include <ocon/node.h>

#include "utils.h"

namespace ocon {


// Root node constructor
Node::Node(Node* parent, const std::string& name, const unsigned& multi_rate_factor) :
        parent_(parent), multi_rate_factor_(multi_rate_factor)
{
    // Node info:
    info_.set_name(name);
    info_.set_multi_rate_factor(multi_rate_factor);
    if (parent == nullptr) {
        // Root node is responsible for creating these pointers
        node_map_ = std::make_shared<node_map_t_>();
        service_ = std::make_shared<Service>(this);
        grpc_services_ = std::make_shared<grpc_services_t_>();
        grpc_services_->emplace_back(service_.get());
    } else {
        // Child node inherits shared pointers from parent
        node_map_ = parent->node_map_;
        service_ = parent->service_;
        grpc_services_ = parent->grpc_services_;
        parent->children_.emplace_back(this);
    }
    // Insert in node tree map and throw on duplicate
    if (!node_map_->insert(std::make_pair(path(), this)).second) {
        throw std::runtime_error("Duplicate child '" + path() + "' found in node tree");
    };
}

void Node::ExecuteInit()
{
    // First pass of initialize
    if (!info_.initialized()) {
        std::lock_guard<std::recursive_mutex> guard(mu_);
        info_.set_type(typeid(*this).name());
        info_.set_has_init_method(true);
        info_.set_has_step_method(true);
        info_.set_has_init_callback_method(true);
        info_.set_has_step_callback_method(true);
        info_.set_initialized(true);
    }
    // Update all bindings owned by current node
    ExecuteSignalBindings(bindings_);
    // Call virtual init method under recursive guard
    if (info_.has_init_method()) {
        LockGuard guard(this);
        Init();
    }
    // Execute init algorithm for all children
    std::for_each(children_.begin(), children_.end(),[](Node* node){
        node->ExecuteInit();
    });
    // Update callback bindings: inputs which are coupled to descendant outputs
    ExecuteSignalBindings(bindings_callback_);
    // Call virtual callback method (if defined)
    if (info_.has_init_callback_method()) {
        LockGuard guard(this);
        InitCallback();
    }
}


void Node::ExecuteStep(const double& time_step)
{
    if (!info_.initialized()) {
        throw std::runtime_error("Node not initialized: call ExecuteInit() before calling ExecuteStep()");
    }
    // Update all bindings owned by current node
    ExecuteSignalBindings(bindings_);
    // Execute step at a multiple of the parent frequency (default = 1)
    double time_step_multi_rate = time_step / multi_rate_factor_;
    for (unsigned i = 0; i < multi_rate_factor_; i++) {
        // Call virtual step method (if defined) under recursive guard (all descendants are locked)
        if (info_.has_step_method()) {
            LockGuard guard(this);
            if (i == 0) info_.set_time_step(time_step_multi_rate);
            Step(time_step_multi_rate);
        } else if (i == 0) {
            LockGuard guard(this, false);
            info_.set_time_step(time_step_multi_rate);
        }
        // Execute step algorithm for all children
        std::for_each(children_.begin(), children_.end(),[&time_step_multi_rate](Node* node){
            node->ExecuteStep(time_step_multi_rate);
        });
        // Update callback bindings: inputs which are coupled to descendant outputs
        ExecuteSignalBindings(bindings_callback_);
    }
    // Call virtual callback method under recursive guard (all descendants are locked)
    if (info_.has_step_callback_method()) {
        LockGuard guard(this);
        StepCallback();
    }
}


void  Node::ExecuteSignalBindings(const bindings_t_& bindings)
{
    for (const auto& binding : bindings) {

        // locking order matters to avoid dead lock.
        std::lock_guard<std::recursive_mutex> guard1(binding.lock_order.first->mu_);
        std::lock_guard<std::recursive_mutex> guard2(binding.lock_order.second->mu_);

        auto target_message = MessageFieldResolver(binding.target_interface->message, binding.target_fields);
        auto source_message = MessageFieldResolver(binding.source_interface->message, binding.source_fields);

        // execute the copy
        target_message->CopyFrom(*source_message);
    }
}


void Node::RegisterInterface(const std::string &name, Message* interface, NodeInterfaceType type)
{
    if (info_.initialized()) {
        throw std::runtime_error("Node already initialized: register interfaces before calling ExecuteInit()");
    }

    // Check for duplicate key
    if (FindInterface(name) != nullptr) {
        throw std::runtime_error("Attempting to register duplicate interface with name: " + name);
    }

    // register the interface
    interfaces_.emplace_back(Interface{name, interface, type});

    // Allocate interface message (required for connect signals)
    AllocateMessages(interface);
}


NodeInfo Node::info() const
{
    std::lock_guard<std::recursive_mutex> guard(mu_);
    return info_;
}


// assembles and returns the node tree message (recursively).
NodeTree Node::tree() const {
    NodeTree result;
    result.set_path(path());
    {
        std::lock_guard<std::recursive_mutex> guard(mu_);
        result.mutable_info()->CopyFrom(info_);
    }
    for (const auto& binding : bindings_) {
        result.add_bindings()->CopyFrom(binding.message);
    }
    for (const auto& interface : interfaces_) {
        auto added_interface = result.add_interfaces();
        added_interface->set_name(interface.name);
        added_interface->set_type(interface.type);
        if (interface.message != nullptr) {
            std::lock_guard<std::recursive_mutex> guard(mu_);
            added_interface->mutable_message()->PackFrom(*interface.message);
        }
    }
    for (const auto& child : children_) {
        result.add_children()->CopyFrom(child->tree());
    }
    return result;
}


Any Node::message(const NodeMessageReference& request) const
{
    Any result;
    std::lock_guard<std::recursive_mutex>guard(mu_);
    auto parse_result = ParseMessageRequest(request);
    if (parse_result.success) {
        result.PackFrom(*parse_result.message);
    }
    return result;
}


std::string Node::path() const
{
    if (parent_ != nullptr) {
        return parent_->path() + '.' + info_.name();
    } else {
        return info_.name();
    }
}


const std::shared_ptr<Node::grpc_services_t_> Node::grpc_services() const
{
    return grpc_services_;
}


void Node::ConnectSignalsImplementation(const Message& source_message, const Message& target_message, bool optimize)
{
    if (info_.initialized()) {
        throw std::runtime_error("Node already initialized: connect signals before calling ExecuteInit()");
    }

    auto source = SearchMessage(source_message);
    auto target = SearchMessage(target_message);

    SignalBinding message;
    message.mutable_source()->CopyFrom(source.reference);
    message.mutable_target()->CopyFrom(target.reference);

    // Some more checks
    if (source.node == target.node) {
        throw std::runtime_error("Binding error: binding source node equals target node. " + message.DebugString());
    }

    // Check source interface type
    std::vector<NodeInterfaceType> source_disallowed_types{INTERNAL, PARAMETER};
    for (auto& type: source_disallowed_types) {
        if (type == source.interface->type) {
            const auto &name = NodeInterfaceType_Name(type);
            throw std::runtime_error("Binding error: source type " + name + " not allowed " + message.DebugString());
        }
    }

    // Check target interface type
    std::vector<NodeInterfaceType> target_disallowed_types{OUTPUT, INTERNAL, PARAMETER };
    for (auto& type: target_disallowed_types) {
        if (type == target.interface->type) {
            const auto &name = NodeInterfaceType_Name(type);
            throw std::runtime_error("Binding error: target type " + name + " not allowed " + message.DebugString());
        }
    }

    // set locking order, e.g. which node should locks first.
    typedef std::pair<const Node*, const Node*> node_pair_t;
    std::function<bool (const Node*, node_pair_t&)> lock_order = [&] (const Node* node, node_pair_t& result) {
        if (node == target.node) {
            result = std::make_pair(target.node, source.node);
            return true;
        }
        if (node == source.node) {
            result = std::make_pair(source.node, target.node);
            return true;
        }
        for (auto& child : node->children_) {
            if (lock_order(child, result)) return true;
        }
        if (node == GetRoot()) {
            throw std::runtime_error("Binding error: unexpected error when setting lock order.");
        }
        return false;
    };

    // Create signal binding
    SignalBindingData binding;
    binding.message = std::move(message);
    lock_order(GetRoot(), binding.lock_order);
    binding.source_interface = source.interface;
    binding.target_interface = target.interface;
    binding.source_fields = source.fields;
    binding.target_fields = target.fields;

    // Determine owner of the signal binding. By default (optimize == true) the target owns the binding
    // and thus the bindings are executed just in time (e.g. minimal latency).
    // If false, the current node owns the binding.
    Node* owner = optimize ? target.node : this;

    // Insert regular binding
    owner->bindings_.emplace_back(binding);

    // Insert callback binding if optimization is enabled and the target is an ancestor of source node
    if (optimize) {
        auto source_node_parent = source.node->parent_;
        while (source_node_parent != nullptr) {
            if (source_node_parent == target.node) {
                owner->bindings_callback_.emplace_back(binding);
                break;
            }
            source_node_parent = source_node_parent->parent_;
        }
    }
}


Node::ParseMessageRequestResult Node::ParseMessageRequest(const NodeMessageReference& request) const
{
    // prepare result, assume success unless error is detected.
    ParseMessageRequestResult result;

    // return error
    auto error = [&result](std::string error_string) {
        result.error = error_string;
        return result;
    };

    // Retrieve the node and parse the provided message path
    if (node_map_->find(request.node_path()) == node_map_->end()) {
        return error("Node not found");
    }
    result.node = node_map_->at(request.node_path());

    // Retrieve the node interface key
    const auto &path = request.message_path();
    auto index = path.find_first_of('.');
    result.key = path.substr(0, std::min(index, path.size()));

    // Find the specified node
    auto interface = FindInterface(result.key);
    if (interface == nullptr) {
        return error("Could not resolve interface key");
    }

    auto message_path = path.substr(std::min(index + 1, path.size()), path.size());
    if (!GenerateFieldsFromPath(interface->message, message_path, result.fields)) {
        return error("Could not resolve message path");
    }

    // Return the requested message by resolving the fields
    result.message = MessageFieldResolver(interface->message, result.fields);
    result.success = true;
    return result;
}


Node::SearchMessageResult Node::SearchMessage(const Message& message) const
{
    SearchMessageResult result;

    // scan through node map search all interfaces recursively
    for (auto& node_map_item : *node_map_) {
        auto& node = node_map_item.second;
        for (auto& interface : node->interfaces_) {
            if (interface.message != nullptr) {
                if (RecursiveMessageSearch(message, *interface.message, result.fields)) {
                    result.node = node;
                    result.interface = &interface;
                    result.reference = CreateNodeMessageReference(node, interface.name, result.fields);
                    return result;
                };
            }
        }
    }
    // throw exception in case of no result
    std::stringstream ss;
    ss << "Could not find message in provided node map at address: " << &message;
    throw std::runtime_error(ss.str());
}


Node::Service::Service(Node* node) : node_(node) {};


grpc::Status Node::Service::GetNodeMessage(grpc::ServerContext *, const NodeMessageReference *request, Any *response) {
    response->CopyFrom(node_->message(*request));
    return grpc::Status::OK;
}


grpc::Status Node::Service::GetNodeTree(grpc::ServerContext *, const Empty *request, NodeTree *response) {
    response->CopyFrom(node_->tree());
    return grpc::Status::OK;
}


Node::LockGuard::LockGuard(const Node *node, bool lock_children) : node_(node), lock_children_(lock_children)
{
    if (lock_children_) {
        node_->GuardTree(&std::recursive_mutex::lock);
    } else {
        node_->mu_.lock();
    }
}


Node::LockGuard::~LockGuard()
{
    if (lock_children_) {
        node_->GuardTree(&std::recursive_mutex::unlock);
    } else {
        node_->mu_.unlock();
    }
}


void Node::GuardTree(void(std::recursive_mutex::* mutex_method)()) const
{
    (mu_.*mutex_method)();
    for (auto& child : children_) {
        child->GuardTree(mutex_method);
    }
}

void Node::RegisterService(grpc::Service *service)
{
    if (info_.initialized()) {
        throw std::runtime_error("Node already initialized: register services before calling ExecuteInit()");
    }

    for (grpc::Service* grpc_service : *grpc_services_) {
        if (typeid(*grpc_service) == typeid(*service)) {
            throw std::runtime_error("Add service error. Identical services can not be registered more than once.");
        }
    }
    grpc_services_->emplace_back(service);
}


void Node::Init()
{
    info_.set_has_init_method(false);
}


void Node::Step(const double& time_step)
{
    info_.set_has_step_method(false);
}


void Node::InitCallback()
{
    info_.set_has_init_callback_method(false);
}


void Node::StepCallback()
{
    info_.set_has_step_callback_method(false);
}


const Node::Interface* Node::FindInterface(const std::string &name) const {
    for (auto &interface: interfaces_) {
        if (name == interface.name) return &interface;
    }
    return nullptr;
}


bool Node::SaveParameters() {
    LockGuard guard(this);
    return (std::all_of(interfaces_.begin(), interfaces_.end(), [&](Interface& interface){
        if (interface.type == NodeInterfaceType::PARAMETER) {
            auto file_name = path() + "." + interface.name + ".bin";
            std::fstream file(file_name, std::ios::binary | std::ios::trunc | std::ios::out);
            return interface.message->SerializeToOstream(&file);
        } else {
            return true;
        }
    }));
}


bool Node::LoadParameters() {
    LockGuard guard(this);
    return (std::all_of(interfaces_.begin(), interfaces_.end(), [&](Interface& interface){
        if (interface.type == NodeInterfaceType::PARAMETER) {
            auto file_name = path() + "." + interface.name + ".bin";
            std::fstream file(file_name, std::ios::binary | std::ios::in);
            return interface.message->ParseFromIstream(&file);
        } else {
            return true;
        }
    }));
}

const Node *Node::GetRoot() const {
    if (parent_ == nullptr) {
        return this;
    } else {
        return parent_->GetRoot();
    }
}



} // namespace ocon
