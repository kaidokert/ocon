/*
 *
 * Copyright 2018 Peter de Lange.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include <math.h>

#include "test_nodes.h"


ControllerDummy::ControllerDummy(Node *parent, const std::string &name, unsigned int multi_rate_factor) :
        NodeTemplate(parent, name, multi_rate_factor)
{
    if (parent == nullptr) {
        RegisterService(this);
    }
}

void ControllerDummy::Step(const double& time_step) {

    // Set output to input bool to check signal propagation
    outputs_->mutable_test_bool_message()->set_test(inputs().test_bool_message().test());
    outputs_->set_time(outputs().time() + float(time_step));
    outputs_->set_iterations(outputs().iterations() + 1);
}

TestDataTypesUpdate::TestDataTypesUpdate(Node *parent, const std::string &name) :
        NodeTemplate(parent, name)
{
    *outputs_->mutable_test_repeated_float()->Add() = 1.f;
    *outputs_->mutable_test_repeated_float()->Add() = 2.f;
    *outputs_->mutable_test_repeated_float()->Add() = 3.f;
}

void TestDataTypesUpdate::Step(const double& time_step) {
    outputs_->set_test_bool(true);
    outputs_->set_test_float(42.f);
    outputs_->set_test_double(26.);
    outputs_->set_test_int32(-32);
    outputs_->set_test_int64(-62);
    outputs_->set_test_uint32(32);
    outputs_->set_test_uint64(62);
    outputs_->set_test_string("Hello World!");
    (*outputs_->mutable_test_string_float_map())["a"] = 4.f;
    (*outputs_->mutable_test_string_float_map())["b"] = 5.f;
    (*outputs_->mutable_test_string_float_map())["c"] = 6.f;
    outputs_->mutable_test_foo()->set_foo(1.f);
    outputs_->mutable_test_bar()->set_bar(2);
}

TestExchangeDataTypes::TestExchangeDataTypes(Node *parent, const std::string &name) :
        NodeTemplate(parent, name),
        c1(this, "VariousDataTypes1"), c2(this, "VariousDataTypes2")
{
    ConnectSignals(c1.outputs(), c2.inputs());
}


ControllerTestFixture::ControllerTestFixture() :
        Node(nullptr, "TestFixture", 2), // multi_rate_factor = 2
        data_types_(this, "DataTypes"),
        controller1_(this, "Controller1"),
        controller2_(this, "Controller2", multi_rate_factor_2_),
        controller3_(&controller2_, "Controller3", multi_rate_factor_3_)
{
}