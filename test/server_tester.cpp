/*
 *
 * Copyright 2018 Peter de Lange.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include <thread>
#include <string>

#include <boost/beast/core.hpp>
#include <boost/beast/websocket.hpp>
#include <boost/asio/connect.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <gtest/gtest.h>
#include <google/protobuf/empty.pb.h>
#include <google/protobuf/util/message_differencer.h>

#include <ocon/server.h>
#include <ocon/ocon_node.grpc.pb.h>
#include <ocon/ocon_server.grpc.pb.h>

#include "test_nodes.h"
#include "server_websocket.h"
#include "server_grpc.h"


using namespace ocon;

class ServerTestFixture {

public:

    ServerConfiguration config_;

    ControllerDummy* node_ = nullptr;
    Server* server_ = nullptr;
    std::shared_ptr<grpc::Channel> channel_ = nullptr;

    google::protobuf::util::MessageDifferencer message_differencer_;

    explicit ServerTestFixture()
    {
        config_.set_host("127.0.0.1");
        config_.set_port_grpc(50051);
        config_.set_port_websocket(8080);

        node_ = new ControllerDummy(nullptr, "Dummy");
        server_ = new Server(node_, config_);

        server_->Run();

        auto host = config_.host() + ":" + std::to_string(config_.port_grpc());
        channel_ = grpc::CreateChannel(host, grpc::InsecureChannelCredentials());

        std::chrono::system_clock::time_point now = std::chrono::system_clock::now() + std::chrono::milliseconds(1000);
        EXPECT_TRUE(channel_->WaitForConnected(now));
        EXPECT_EQ(channel_->GetState(true), GRPC_CHANNEL_READY);
    }

    ~ServerTestFixture() {
        delete server_;
        delete node_;
    }

    ServerConfiguration server_config() {
        return server_->config();
    }

};


class WebSocketClient {

    // The io_context is required for all I/O
    boost::asio::io_context ioc_;
    boost::beast::websocket::stream<boost::asio::ip::tcp::socket> ws_;

public:
    explicit WebSocketClient(const ServerConfiguration &config) : ws_{ioc_}
    {
        tcp::resolver resolver{ioc_};
        auto const results = resolver.resolve(config.host(), std::to_string(config.port_websocket()));
        boost::asio::connect(ws_.next_layer(), results.begin(), results.end());
        ws_.handshake(config.host(), "/");
    }

    ~WebSocketClient() {
        ws_.close(boost::beast::websocket::close_code::normal);
    }

    std::string WriteAndRead(const std::string &message) {
        ws_.write(boost::asio::buffer(message));
        boost::beast::multi_buffer buffer;
        ws_.read(buffer);
        return boost::beast::buffers_to_string(buffer.data());
    }
};


// Sends a WebSocket message and prints the response
TEST(ControllerTest, TestServerWebSocket) {

    // Dummy echo method
    auto method = [](const std::string& request, std::string& response) -> bool {
        response = request;
        return true;
    };

    ServerConfiguration config;
    config.set_host("127.0.0.1");
    config.set_port_websocket(8081);
    ServerWebSocket fixture(method, config);
    fixture.Run();

    // Client
    WebSocketClient client(config);
    std::string request = "Hi Mark!";
    std::string response = client.WriteAndRead(request);

    EXPECT_EQ(request, response);
}


TEST(ControllerTest, NodeServerConfigDefaultValues) {

    ControllerDummy model (nullptr, "Model");

    // Empty config should give default values
    ServerConfiguration config;
    Server server_default (&model, config);
    EXPECT_EQ(server_default.config().host(), "127.0.0.1");
    EXPECT_EQ(server_default.config().port_websocket(), 8080);
    EXPECT_EQ(server_default.config().port_grpc(), 50051);

    // Partial config should give default values except the specified parameters
    config.set_port_grpc(1);
    Server server_partial(&model, config);
    EXPECT_EQ(server_default.config().host(), "127.0.0.1");
    EXPECT_EQ(server_partial.config().port_websocket(), 8080);
    EXPECT_EQ(server_partial.config().port_grpc(), 1);

}

TEST(ControllerTest, NodeServerService) {

    ServerTestFixture fixture;
    auto stub = ServerService::NewStub(fixture.channel_);

    // Empty config should give default values
    Empty request;
    ServerInfo response;

    // GetServerInfo
    grpc::ClientContext ctx;
    ASSERT_TRUE(stub->GetServerInfo(&ctx, request, &response).ok());
    EXPECT_EQ(response.status(), RUNNING);

}


TEST(ControllerTest, NodeServerGRPC) {

    ServerTestFixture fixture;

    auto stub = NodeService::NewStub(fixture.channel_);

    // request
    NodeMessageReference request;
    request.set_node_path(fixture.node_->path());
    request.set_message_path("outputs.test_foo");

    // expected response
    google::protobuf::Any response_expected;
    response_expected.PackFrom(fixture.node_->outputs().test_foo());

    // check if response is not empty non initialized
    EXPECT_FALSE(fixture.message_differencer_.Compare(response_expected, google::protobuf::Any()));

    // response check non grpc
    auto response_non_grpc = fixture.node_->message(request);
    EXPECT_TRUE(fixture.message_differencer_.Compare(response_expected, response_non_grpc));
    // response grpc
    grpc::ClientContext ctx_node_output;
    google::protobuf::Any response_grpc;
    EXPECT_TRUE(stub->GetNodeMessage(&ctx_node_output, request, &response_grpc).ok());
    EXPECT_TRUE(fixture.message_differencer_.Compare(response_expected, response_grpc));

    // Check node tree grpc method
    grpc::ClientContext ctx_node_tree;
    NodeTree response_node_tree_grpc;
    EXPECT_TRUE(stub->GetNodeTree(&ctx_node_tree, Empty(), &response_node_tree_grpc).ok());
    EXPECT_TRUE(fixture.message_differencer_.Compare(fixture.node_->tree(), response_node_tree_grpc));

    // Finally check channel status
    EXPECT_EQ(fixture.channel_->GetState(false), GRPC_CHANNEL_READY);

    // Check if the other service is working as well
    auto test_stub = TestService::NewStub(fixture.channel_);

    // request
    auto request_name = NameMessage();
    auto response_name = NameMessage();

    grpc::ClientContext ctx_test_name;
    request_name.set_name("mark");
    EXPECT_TRUE(test_stub->EchoName(&ctx_test_name, request_name, &response_name).ok());
    EXPECT_TRUE(fixture.message_differencer_.Compare(request_name, response_name));

}

TEST(ControllerTest, NodeServerGRPC_multiple_client_threads) {

    google::protobuf::util::MessageDifferencer message_differencer;

    ServerTestFixture fixture;

    auto stub = NodeService::NewStub(fixture.channel_);

    auto get_node_messages_test = [&]() {
        grpc::ClientContext context;
        NodeMessageReference request;
        request.set_node_path(fixture.node_->path());
        request.set_message_path("outputs.test_foo");
        google::protobuf::Any response;
        auto response_non_grpc = fixture.node_->message(request);
        ASSERT_FALSE(message_differencer.Compare(response, response_non_grpc));
        auto status_get_node_messages = stub->GetNodeMessage(&context, request, &response);
        ASSERT_TRUE(status_get_node_messages.ok());
        ASSERT_TRUE(message_differencer.Compare(response, response_non_grpc));
    };

    // Execute many times
    const int number_of_threads = 5;
    const int number_of_executions_per_thread = 20;
    std::vector<std::thread> threads;
    for (int i = 0; i < number_of_threads; i++) {
        threads.emplace_back(std::thread([&](std::shared_ptr<grpc::Channel> channel) {
            for (int j = 0; j < number_of_executions_per_thread; j++) {
                get_node_messages_test();
            }
        }, fixture.channel_));
    }
    for (auto &thread: threads) {
        thread.join();
    }

}

TEST(ControllerTest, StartChannelBeforeServer) {

    ServerConfiguration config;
    config.set_host("127.0.0.1");
    config.set_port_grpc(50051);

    // start channel before server and check connection
    auto host = config.host() + ":" + std::to_string(config.port_grpc());
    auto channel = grpc::CreateChannel(host, grpc::InsecureChannelCredentials());
    EXPECT_EQ(channel->GetState(false), GRPC_CHANNEL_IDLE);

    // start server
    ControllerDummy node (nullptr, "TestGRPC", 2);
    Server server (&node, config);
    server.Run();

    // wait up to 1 second for a connection
    std::chrono::system_clock::time_point now = std::chrono::system_clock::now() + std::chrono::milliseconds(1000);
    ASSERT_TRUE(channel->WaitForConnected(now));
    EXPECT_EQ(channel->GetState(true), GRPC_CHANNEL_READY);

}