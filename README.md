# OCON

C++ distributed open source controller library.

Product page: https://oconomy.gitlab.io/page/

## Installation (Linux)

- Tested on Ubuntu 18.04 LTS and Raspbian GNU/Linux 9
- gcc 6.3.0 or higher
- CMake 3.7 (recommended 3.12 for better boost 1.68 compatibility)
- Boost 1.68.0 or higher (https://www.boost.org/users/history/version_1_68_0.html)

Required packages:

``` bash
[sudo] apt install build-essential cmake openssl-dev golang-go autoconf libtool pkg-config libgflags-dev libgtest-dev
```

Build and install gRPC from source with cmake:

``` bash
# Clone gRPC repository and submodules
git clone https://github.com/grpc/grpc.git && cd grpc && git submodule init && git submodule update --init --recursive
# Install all gRPC dependencies (includes protobuf and google test)
mkdir -p cmake/build && cd cmake/build
cmake -DgRPC_INSTALL=OFF -DgRPC_BUILD_TESTS=OFF -DCMAKE_BUILD_TYPE=Release ../.. && make
[sudo] make install
cmake -DgRPC_INSTALL=ON -DgRPC_BUILD_TESTS=OFF -DgRPC_PROTOBUF_PROVIDER=package -DgRPC_ZLIB_PROVIDER=package -DgRPC_CARES_PROVIDER=package -DgRPC_SSL_PROVIDER=package -DCMAKE_BUILD_TYPE=Release ../..  && make
[sudo] make install
```
### Build and install OCON from source
``` bash
# Clone git repository
git clone https://gitlab.com/oconomy/ocon.git && cd ocon
# Build the project
mkdir -p cmake/build && cd cmake/build
cmake -DCMAKE_BUILD_TYPE=Release ../..  && make
# Install
[sudo] make install
```
## Examples
Examples include a single node [pendulum](examples/pendulum) and a multi node [bicycle controller](examples/bicycle).
## Run tests
Execute the tester from the cmake build directory:
``` bash
test/ocon_tester
```
