class NodeTree {

  String path;
  List<int> selector = [];
  Map<String, dynamic> info;
  List<NodeTree> children = null;
  Map<String, dynamic> interfaces = null;
  Map<String, dynamic> server;
  List<dynamic> bindings;

  // map interface children by name
  Map<String, int> _map_children = null;
  NodeTree get_child(String name) {
    if (children != null) {
      return children[_map_children[name]];
    } else {
      return null;
    }
  }

  // returns node from path specification
  NodeTree from_path(String path) {
    NodeTree result = this;
    path.split('.').sublist(1).forEach((v) {
      if (result == null) {
        return null;
      }
      result = result.get_child(v);
    });
    return result;
  }

  // Generates more strongly typed node tree based on json serialized object
  NodeTree(Map<String, dynamic> raw_node, [List<int> selector = null]) {
    path = raw_node['path'];
    info = raw_node['info'];
    interfaces = {};
    raw_node['interfaces'].forEach((interface) {
      interfaces[interface['name']] = interface['message'];
    });
    server = raw_node['server'];
    bindings = raw_node['bindings'];
    if (selector != null) {
      this.selector = selector;
    }
    if (raw_node['children'] != null) {
      children = []; _map_children = {};
      List<dynamic> raw_node_children = raw_node['children'];
      for (var i = 0; i < raw_node_children.length; i++) {
        Map<String, dynamic> raw_node_child = raw_node_children[i];
        _map_children[raw_node_child['info']['name']] = i;
        children.add(NodeTree(raw_node_child, this.selector + [i]));
      }
    }
  }
}