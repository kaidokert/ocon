import 'package:angular/angular.dart';
import 'package:ocon_gui/app_component.template.dart' as ng;

void main() {
  runApp(ng.AppComponentNgFactory);
}