/*
 *
 * Copyright 2018 Peter de Lange.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#ifndef OCON_SERVER_H
#define OCON_SERVER_H

#include <pthread.h>

#include <grpcpp/generic/async_generic_service.h>

#include <ocon/common.h>
#include <ocon/ocon_server.pb.h>
#include <ocon/ocon_server.grpc.pb.h>

namespace ocon {

class ServerWebSocket;
class ServerGRPC;

// OCON server. Wraps an OCON node into a server object.
// The server provides three basic services:
// - Real time scheduler (optional configurable)
// - gRPC server
// - Websocket server (for OCON GUI communication)
//
class Server final : public ServerService::Service {

    // Mutex for protecting server data structures among threads
    mutable std::mutex mu_;

    // Server configuration structure
    ServerConfiguration config_;

    // Real time scheduler
    pthread_t rt_thread_;
    struct sched_param rt_thread_param_{};
    pthread_attr_t rt_thread_attrib_;
    std::atomic<bool> thread_running_{false};

    // Pointer to (root) node
    Node* root_node_;

    // Websocket and gRPC server
    std::unique_ptr<ServerWebSocket> server_ws_;
    std::unique_ptr<ServerGRPC> server_grpc_;

    ServerInfo server_info_;

public:

    // Initialize server with server configuration and (root) node pointer
    explicit Server(Node *root_node, ServerConfiguration config = {});

    Server(const Server&) = delete;
    Server& operator=(const Server&) = delete;
    ~Server() override;

    // Start the server, e.g. scheduler (if configured), websocket, and gRPC.
    void Run();

    // Public data accessors
    const ServerConfiguration& config() const;
    ServerInfo server_info () const;

    // gRCP services implementation
    grpc::Status GetServerInfo(grpc::ServerContext*, const Empty* request, ServerInfo* response) override;

private:

    // Real time scheduler thread method
    static void* RealTimeScheduler(void* data);

    // Websocket method for handling requests and returning data
    bool WebSocketStreamData(const std::string &request, std::string &response);

};

} // namespace ocon

#endif //OCON_SERVER_H
