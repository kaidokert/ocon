/*
 *
 * Copyright 2018 Peter de Lange.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#ifndef OCON_NODE_H
#define OCON_NODE_H

#include <ocon/common.h>
#include <ocon/ocon_node.pb.h>
#include <ocon/ocon_node.grpc.pb.h>


namespace ocon {

// Virtual base class for OCON node
//
// An OCON node can either be a initialized as parent or child dependent on which constructor is called.
//
// Main (virtual) methods:
//
// - Init(): called during init execution
// - Step(const double& dt): called during step execution
// - InitCallback(): called during init execution after init execution of its children.
// - StepCallback(): called during step execution after step execution of its children.
//
// Use RegisterInterface(Message*) to register and allocate interface messages
// Use ConnectSignals(Message*, Message*) to connect messages between nodes
//
class Node {

    // Mutex lock for accessing / writing node data members.
    // Lock strategy is performed on a per node base.
    mutable std::recursive_mutex mu_;

    // node tree structure with flat node map for quick access.
    // const Node* root_ = nullptr;
    const Node* parent_ = nullptr;
    std::vector<Node*> children_;
    typedef std::map<std::string, Node*> node_map_t_;
    std::shared_ptr<node_map_t_> node_map_ = nullptr;

    // Execute node and child nodes by multiple rate of the parent time interval [-]
    const unsigned multi_rate_factor_;

    // Basic node information like name, type, time step, and so on.
    NodeInfo info_ = NodeInfo();

    // Node interfaces. Use RegisterInterface(Message*) to push back new interfaces
    struct Interface {
        const std::string name;
        Message* message;
        const NodeInterfaceType type;
    };
    std::vector<Interface> interfaces_;

    // Signal binding data structure.
    struct SignalBindingData {
        SignalBinding message;
        std::pair<const Node*, const Node*> lock_order;
        Interface* source_interface;
        Interface* target_interface;
        Fields_t source_fields;
        Fields_t target_fields;
    };
    typedef std::vector<SignalBindingData> bindings_t_;
    bindings_t_ bindings_;
    bindings_t_ bindings_callback_;

    // Node service implementation.
    class Service : public NodeService::Service {
        Node* node_;
    public:
        explicit Service(Node* node);
        grpc::Status GetNodeMessage(grpc::ServerContext*, const NodeMessageReference* request, Any* response) override;
        grpc::Status GetNodeTree(grpc::ServerContext*, const Empty* request, NodeTree* response) override;
    };
    std::shared_ptr<Service> service_ = nullptr;

    // grpc services
    typedef std::vector<grpc::Service*> grpc_services_t_;
    std::shared_ptr<grpc_services_t_> grpc_services_ = nullptr;

public:

    // Constructor, copy move and destructor
    Node(Node* parent, const std::string& name, const unsigned& multi_rate_factor = 1);
    Node(const Node&) = delete;
    Node& operator=(const Node&) = delete;
    virtual ~Node() = default;

    void ExecuteStep(const double& time_step); // Step node and all its children recursively
    void ExecuteInit(); // Init node and all its children recursively

    // Signal binding. Use this method to bind messages across different nodes within the same tree.
    // Note: signals need to be registered and allocated first by using RegisterInterface(Message*)
    // Use optimize argument to ensures minimal latency (optimize = true).
    template<class TMessage>
    void ConnectSignals(const TMessage& source, const TMessage& target, bool optimize = true) {
        static_assert(std::is_base_of<Message, TMessage>::value, "Source and target are not a message type");
        if (&TMessage::default_instance() == &source)
            throw std::runtime_error("Source is not an allocated interface.");
        if (&TMessage::default_instance() == &target)
            throw std::runtime_error("Target is not an allocated interface.");
        ConnectSignalsImplementation(source, target, optimize);
    }

    bool SaveParameters();
    bool LoadParameters();

    // Public data accessors
    std::string path() const; // returns the current node path, e.g. RootNode.Child2.SubChild1
    Any message(const NodeMessageReference& request) const; // retrieve arbitrary node (interface) messages
    NodeTree tree() const; // returns a recursive tree data structure.
    NodeInfo info() const; // returns node information such as name, type, time_step, and so on.
    const std::shared_ptr <grpc_services_t_> grpc_services() const; // returns node tree grpc services

protected:
    // Guard class similar to std::lock_guard which locks node and all its descendants
    class LockGuard {
        const Node* node_; const bool lock_children_;
    public:
        explicit LockGuard(const Node* node, bool lock_children = true);
        ~LockGuard();
    };

    // Register node interfaces.
    void RegisterInterface(const std::string &name, Message* interface, NodeInterfaceType = UNDEFINED);

    // Register a gRPC service. A service type can only be registered once.
    void RegisterService(grpc::Service *service);

private:
    virtual void Init(); // User definable init method
    virtual void Step(const double& dt); // User definable step method
    virtual void InitCallback(); // User definable init callback method
    virtual void StepCallback(); // User definable step callback method

    // Executes signal bindings under node guard. Called at start of ExecuteStep method.
    void ExecuteSignalBindings(const bindings_t_& bindings);
    void ConnectSignalsImplementation(const Message& source, const Message& target, bool optimize);

    // Result data structure of parse message request.
    struct ParseMessageRequestResult {
        bool success = false;
        std::string error = std::string();
        Node* node = nullptr;
        Message* message;
        std::string key;
        Fields_t fields;
    };
    ParseMessageRequestResult ParseMessageRequest(const NodeMessageReference& request) const;

    // Auxiliary method used for connecting signals.
    // Searches for a message by address inside the entire controller tree
    // Returns data structure on success. Throws runtime errors on failure.
    struct SearchMessageResult {
        Node* node;
        Interface* interface;
        Fields_t fields;
        NodeMessageReference reference;
    };
    SearchMessageResult SearchMessage(const Message& message) const;

    // Recursive method for locking / unlocking current node and its descendants. Used primarily by LockGuard(Node*)
    void GuardTree(void(std::recursive_mutex::* mutex_method)()) const;

    // Find interface by name. returns null pointer when the interface could not be located.
    const Interface* FindInterface(const std::string& name) const;

    // Returns root node
    const Node* GetRoot() const;

};


} // namespace ocon

#endif //OCON_NODE_H
