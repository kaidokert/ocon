/*
 *
 * Copyright 2018 Peter de Lange.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#ifndef OCON_NODE_INTERFACE_H
#define OCON_NODE_INTERFACE_H

#include <ocon/node.h>

namespace ocon {

/// Derived template class of node base class with 4 data interfaces:
/// - inputs
/// - outputs
/// - internals (for debugging or monitoring internal variables)
/// - parameters (tunable constants) TODO: implement parameter save, load, and reset mechanism
template<class inputs_t, class outputs_t, class internals_t, class parameters_t>
class NodeTemplate : public Node {

    static_assert(std::is_base_of<Message, inputs_t>(), "Inputs type is not a message.");
    static_assert(std::is_base_of<Message, outputs_t>(), "Outputs type is not a message.");
    static_assert(std::is_base_of<Message, internals_t>(), "Internals type is not a message.");
    static_assert(std::is_base_of<Message, parameters_t>(), "Parameters type is not a message.");

protected:

    google::protobuf::Arena arena_;

    inputs_t* inputs_;
    outputs_t* outputs_;
    internals_t* internals_;
    parameters_t* parameters_;

public:

    // Root node constructor
    NodeTemplate(Node* parent, const std::string& name, const unsigned& multi_rate_factor = 1) :
            Node(parent, name, multi_rate_factor),
            inputs_(google::protobuf::Arena::CreateMessage<inputs_t>(&arena_)),
            outputs_(google::protobuf::Arena::CreateMessage<outputs_t>(&arena_)),
            internals_(google::protobuf::Arena::CreateMessage<internals_t>(&arena_)),
            parameters_(google::protobuf::Arena::CreateMessage<parameters_t>(&arena_))
    {
        RegisterInterfaces();
    }

    // public accessors
    const inputs_t& inputs() const {return *inputs_;}
    const outputs_t& outputs() const {return *outputs_;}
    inputs_t* mutable_inputs() const {return inputs_;}

protected:

    const parameters_t& parameters() {return *parameters_;}

private:

    void RegisterInterfaces() {
        RegisterInterface("inputs", inputs_, INPUT);
        RegisterInterface("outputs", outputs_, OUTPUT);
        RegisterInterface("internals", internals_, INTERNAL);
        RegisterInterface("parameters", parameters_, PARAMETER);
    }

};

} // namespace ocon

#endif //OCON_NODE_INTERFACE_H
