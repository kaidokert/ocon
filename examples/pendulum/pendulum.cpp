/*
 *
 * Copyright 2018 Peter de Lange.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include <chrono>
#include <math.h>

#include <ocon/node_template.h>
#include <ocon/server.h>

#include "pendulum.pb.h"
#include "pendulum.grpc.pb.h"

using namespace ocon;
using namespace pendulum;

/* Equations of motion of a pendulum
 *
 *     .
 *     |\        g
 *     |_\ l     |
 *     |θ \
 *     |   o m
 *
 *   m : point mass [kg]
 *   l : length of pendulum [m]
 *   g : gravity [m/s^2]
 *   θ : angle [rad]
 *   u : torque [Nm]
 */
class Pendulum : public NodeTemplate<Inputs, State, Empty, Parameters>, PendulumService::Service {

public:
    explicit Pendulum(Node* parent, const std::string& name) : NodeTemplate(parent, name)
    {
        RegisterService(this);
    }

    grpc::Status SetInputs(grpc::ServerContext* context, const Inputs* request, Empty* response) override {
        LockGuard guard(this);
        inputs_->CopyFrom(*request);
        return grpc::Status::OK;
    };

    grpc::Status GetState(grpc::ServerContext* context, const Empty* request, State* response) override {
        LockGuard guard(this);
        response->CopyFrom(*outputs_);
        return grpc::Status::OK;
    };

    void Init() override {
        outputs_->Clear();
        parameters_->set_m(1.00);
        parameters_->set_g(9.81);
        parameters_->set_l(1.00);
    }

    void Step(const double& time_step) override {

        // map in
        auto theta = outputs().theta();
        auto theta_dot = outputs().theta_dot();
        auto u = inputs().torque();
        auto g = parameters_->g();
        auto m = parameters_->m();
        auto l = parameters_->l();

        // state integration (simple 1st order Euler method)
        theta_dot += (-g/l*sin(theta) + u/(m*pow(l, 2.))) * time_step;
        theta += theta_dot * time_step;

        // map out
        outputs_->set_theta(theta);
        outputs_->set_theta_dot(theta_dot);
    }
};


int main () {

    // model
    Pendulum pendulum(nullptr, "MyPendulum");

    // server
    ServerConfiguration config;
    config.set_scheduler(true);
    config.set_time_step(0.01);
    Server server(&pendulum, config);

    // run for 10 seconds
    server.Run();
    std::this_thread::sleep_for(std::chrono::seconds(10));
}