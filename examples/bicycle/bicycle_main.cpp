/*
 *
 * Copyright 2018 Peter de Lange.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include <chrono>

#include <boost/program_options.hpp>

#include <ocon/server.h>

#include "fixture.h"

using namespace ocon;
using namespace bicycle;


int main(int argc, char** argv) {

    // server configuration
    namespace po = boost::program_options;
    po::options_description desc("Allowed options");
    desc.add_options()
            ("host",  po::value<std::string>()->default_value("0.0.0.0"), "hostname or ip address")
            ("port_grpc",  po::value<int>()->default_value(50051), "port number of grpc server")
            ("port_websocket",  po::value<int>()->default_value(8080), "port number of websocket server")
            ("scheduler", po::value<bool>()->default_value(true), "enable real time scheduler")
            ("time_step", po::value<double>()->default_value(0.01), "time step in seconds")
            ;
    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);
    ServerConfiguration config;
    if (vm.count("host"))
        config.set_host(vm["host"].as<std::string>());
    if (vm.count("port_grpc"))
        config.set_port_grpc(vm["port_grpc"].as<int>());
    if (vm.count("port_websocket"))
        config.set_port_websocket(vm["port_websocket"].as<int>());
    if (vm.count("scheduler"))
        config.set_scheduler(vm["scheduler"].as<bool>());
    if (vm.count("time_step"))
        config.set_time_step(vm["time_step"].as<double>());

    // bicycle_fixture model
    BicycleFixture bicycle("bicycle_fixture");

    // initial conditions (stable velocity and initial roll angle perturbation)
    grpc::ServerContext ctx;
    ResetRequest request;
    request.mutable_bicycle_state()->set_velocity(5.);
    request.mutable_bicycle_state()->set_roll_angle(0.2);
    ExternalOutputs response;
    bicycle.Reset(&ctx, &request, &response);

    // server config
    Server server (&bicycle, config);

    // Go!
    server.Run();

    // Time to play and demo around
    while(true) {};

}