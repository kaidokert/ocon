/*
 *
 * Copyright 2018 Peter de Lange.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#include <math.h>

#include "target.h"
#include "bicycle.grpc.pb.h"

namespace bicycle {


Target::Target(ocon::Node *parent, const std::string &name) :
        NodeTemplate(parent, name)
{
}

void Target::Init() {

    outputs_ = inputs_;

}


} // namespace bicycle
