/*
 *
 * Copyright 2018 Peter de Lange.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */


#include <math.h>
#include <functional>
#include <cmath>

#include <google/protobuf/empty.pb.h>

#include "equations_of_motion.h"


namespace bicycle {


using namespace google::protobuf;


BicycleEOM::BicycleEOM(Node* parent, const std::string& name, unsigned multi_rate_factor) :
        NodeTemplate(parent, name, multi_rate_factor)
{
    Init();
}

void BicycleEOM::Step(const double& dt) {

    const int state_length = 8;
    v_double_t_ state(state_length); // state vector

    // map in
    state[0] = outputs().state().roll_rate();
    state[1] = outputs().state().steering_rate();
    state[2] = outputs().state().roll_angle();
    state[3] = outputs().state().steering_angle();
    state[4] = outputs().state().velocity();
    state[5] = outputs().state().x();
    state[6] = outputs().state().y();
    state[7] = outputs().state().psi();

    // ode step (runge kutta 4th order)
    using namespace std::placeholders;
    stepper_.do_step(std::bind(&BicycleEOM::ODE, this, _1, _2, _3), state, 0.0, dt);

    // calculate state derivative
    v_double_t_ state_dot(state_length); // state derivative vector
    ODE(state, state_dot, 0.0);

    // state outputs
    outputs_->mutable_state()->set_roll_rate(state[0]);
    outputs_->mutable_state()->set_steering_rate(state[1]);
    outputs_->mutable_state()->set_roll_angle(state[2]);
    outputs_->mutable_state()->set_steering_angle(state[3]);
    outputs_->mutable_state()->set_velocity(state[4]);
    outputs_->mutable_state()->set_x(state[5]);
    outputs_->mutable_state()->set_y(state[6]);
    outputs_->mutable_state()->set_psi(state[7]);

    // other outputs
    outputs_->set_dxdt(outputs().state().velocity()*cos(outputs().state().psi()));
    outputs_->set_dydt(outputs().state().velocity()*sin(outputs().state().psi()));
    outputs_->set_psi_dot(state_dot[7]);

    // crash if roll angle > 45 degrees
    if (abs(outputs().state().roll_angle()) > (M_PI / 4.)) {
        outputs_->set_crash(true);
    }
}

void BicycleEOM::Init() {

    inputs_->mutable_forces()->Clear();

    outputs_->Clear();
    outputs_->mutable_state()->CopyFrom(inputs_->initial_state());

    parameters_->set_w(1.02);
    parameters_->set_c(0.08);
    parameters_->set_lambda(M_PI / 10.);
    parameters_->set_g(9.81);
    parameters_->set_rr(0.3);
    parameters_->set_mr(2.);
    parameters_->set_irxx(0.0603);
    parameters_->set_iryy(0.12);
    parameters_->set_xb(0.3);
    parameters_->set_zb(-0.9);
    parameters_->set_mb(85.0);
    parameters_->set_ibxx(9.2);
    parameters_->set_ibyy(11);
    parameters_->set_ibzz(2.8);
    parameters_->set_ibxz(2.4);
    parameters_->set_xh(0.9);
    parameters_->set_zh(-0.7);
    parameters_->set_mh(4.0);
    parameters_->set_ihxx(0.05892);
    parameters_->set_ihyy(0.06);
    parameters_->set_ihzz(0.00708);
    parameters_->set_ihxz(-0.00756);
    parameters_->set_rf(0.35);
    parameters_->set_mf(3);
    parameters_->set_ifxx(0.1405);
    parameters_->set_ifyy(0.28);
    parameters_->set_tr(0.);
    parameters_->set_tf(0.);

}

void BicycleEOM::ODE(const BicycleEOM::v_double_t_ &x, BicycleEOM::v_double_t_ &dxdt, const double t) {

    // Map in some state, input, parameter variables for local use
    const auto& par = *parameters_;
    const double& delta_dot = x[1]; // steering rate
    const double& delta = x[3]; // steering angle
    const double& v = x[4]; // velocity
    double pedal_torque = inputs().forces().pedal_torque();

    // The total mass and the corresponding centre of mass location (with respect to the
    // rear contact point P) are
    double mt = par.mb() + par.mf() + par.mr() + par.mh();
    double xt = (par.xb()*par.mb() + par.xh()*par.mh() + par.w()*par.mf())/mt;
    double zt = (-par.rr()*par.mr() + par.zb()*par.mb() + par.zh()*par.mh() - par.rf()*par.mf())/mt;
    // For the system as a whole, the relevant mass moments and products of inertia with
    // respect to the rear contact point P along the global axes are
    double itxx = par.irxx() + par.ibxx() + par.ihxx() + par.ifxx() +
            par.mr()*std::pow(par.rr(), 2.) + par.mb()*std::pow(par.zb(), 2.) +
            par.mh()*std::pow(par.zh(), 2.) + par.mf()*std::pow(par.rf(), 2.);
    double itxz = par.ibxz() + par.ihxz() - par.mb()*par.xb()*par.zb() -
            par.mh()*par.xh()*par.zh() + par.mf()*par.w()*par.rf();
    // The dependent moments of inertia for the axisymmetric rear wheel and front wheel are
    double irzz = par.irxx();
    double ifzz = par.ifxx();
    // Then the moment of inertia for the whole bicycle along the z-axis is
    double itzz = irzz + par.ibzz() + par.ihzz() + ifzz +
            par.mb()*std::pow(par.xb(), 2.) + par.mh()*std::pow(par.xh(), 2.) + par.mf()*std::pow(par.w(), 2.);
    double ma = par.mh() + par.mf();
    // The same properties are similarly defined for the front assembly A
    double xa = (par.xh()*par.mh() + par.w()*par.mf()) / ma;
    double za = (par.zh()*par.mh() - par.rf()*par.mf()) / ma;
    // The relevant mass moments and products of inertia for the front assembly with
    // respect to the centre of mass of the front assembly along the global axes are
    double iaxx = par.ihxx() + par.ifxx() + par.mh()*std::pow(par.zh() - za, 2.) + par.mf()*std::pow(par.rf() + za, 2.);
    double iaxz = par.ihxz() - par.mh()*(par.xh() - xa)*(par.zh() - za) + par.mf()*(par.w() - xa)*(par.rf() + za);
    double iazz = par.ihzz() + ifzz + par.mh()*std::pow(par.xh() - xa, 2.) + par.mf()*std::pow(par.w() - xa, 2.);
    // The centre of mass of the front assembly is ahead of the steering axis by
    // perpendicular distance
    double ua = (xa - par.w() - par.c())*std::cos(par.lambda()) - za*std::sin(par.lambda());
    // For the front assembly three special inertia quantities are needed: the moment of
    // inertia about the steer axis and the products of inertia relative to crossed, skew axes,
    // taken about the points where they intersect. The latter give the torque about one
    // axis due to angular acceleration about the other. For example, the λx component is
    // taken about the point where the steer axis intersects the ground plane. It includes
    // a part from IA operating on unit vectors along the steer axis and along x, and also
    // a parallel axis term based on the distance of mA from each of those axes.
    double ialambdalambda = ma*std::pow(ua, 2.) + iaxx * std::pow(std::sin(par.lambda()), 2.) +
            2.*iaxz*std::sin(par.lambda())*std::cos(par.lambda()) + iazz*std::pow(std::cos(par.lambda()), 2.);
    double ialambdax = -ma*ua*za + iaxx*std::sin(par.lambda()) + iaxz*std::cos(par.lambda());
    double ialambdaz =  ma*ua*xa + iaxz*std::sin(par.lambda()) + iazz*std::cos(par.lambda());
    // The ratio of the mechanical trail (i.e., the perpendicular distance that the front
    // wheel contact point is behind the steering axis) to the wheel base is
    double mu = par.c()/par.w()*cos(par.lambda());
    // The rear and front wheel angular momenta along the y-axis, divided by the forward
    // speed, together with their sum form the gyrostatic coefficients:
    double sr = par.iryy()/par.rr();
    double sf = par.ifyy()/par.rf();
    double st = sr + sf;
    // We define a frequently appearing static moment term as
    double sa = ma*ua + mu*mt*xt;

    // Inertial, damping, and stiffness matrices
    M_(0,0) = itxx;
    M_(0,1) = ialambdax + mu*itxz,
    M_(1,0) = ialambdax + mu*itxz;
    M_(1,1) = ialambdalambda + 2.*mu*ialambdaz + std::pow(mu, 2.)*itzz;
    K0_(0,0) = mt*zt;
    K0_(0,1) = -sa;
    K0_(1,0) = -sa;
    K0_(1,1) = -sa*std::sin(par.lambda());
    K2_(0,0) = 0.;
    K2_(0,1) = ((st - mt*zt)/par.w())*std::cos(par.lambda());
    K2_(1,0) = 0.;
    K2_(1,1) = ((sa + sf*std::sin(par.lambda()))/par.w())*std::cos(par.lambda());
    C1_(0,0) = 0.;
    C1_(0,1) = mu*st + sf*std::cos(par.lambda()) + itxz/par.w()*std::cos(par.lambda()) - mu*mt*zt;
    C1_(1,0) = -(mu*st + sf*std::cos(par.lambda()));
    C1_(1,1) = ialambdaz/par.w()*std::cos(par.lambda()) + mu*(sa + (itzz/par.w())*std::cos(par.lambda()));

    // state and input mapping
    Eigen::Map<const Eigen::Vector4d> state(x.data());
    Eigen::Vector2d u; u << inputs().forces().roll_torque(), inputs().forces().steer_torque();

    auto C = C1_*v;
    auto K = K0_*par.g() + K2_*pow(v, 2.0);
    auto M = M_.ldlt();

    // SS system: dxdt = A*x + B*u
    A_ << -M.solve(C), -M.solve(K), Eigen::Matrix2d::Identity(), Eigen::Matrix2d::Zero();
    Eigen::Vector4d Bu; // B*u
    Bu << M.solve(u), Eigen::Vector2d::Zero();
    Eigen::VectorXd state_derivative = A_*state + Bu;

    // Map outputs on state derivative vector
    Eigen::VectorXd::Map(&dxdt[0], state_derivative.size()) = state_derivative;

    // velocity and xy-coordinates time derivatives
    dxdt[4] = pedal_torque / (mt);
    dxdt[5] = v*cos(x[7]); // rear wheel contact x coordinate time derivative
    dxdt[6] = v*sin(x[7]); // rear wheel contact y coordinate time derivative

    // ψ˙ = ((vδ + c˙δ)/w) cos λ (heading time derivative)
    dxdt[7] = (v*delta + par.c()*delta_dot)/par.w()*cos(par.lambda()); // yaw rate
}

} // namespace bicycle
