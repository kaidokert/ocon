cmake_minimum_required(VERSION 3.7)

project(bicycle_node)

# Package dependencies
if(NOT OCON_AS_SUBMODULE)
    find_package(ocon REQUIRED)
endif()
find_package(Boost COMPONENTS program_options REQUIRED )
find_package(GTest REQUIRED)
find_package(Eigen3 REQUIRED)

# Generate proto interfaces
set(PROTO_FILES bicycle.proto)
ocon_protobuf_generate_cpp(PROTO_SRCS PROTO_HDRS ${PROTO_FILES})

# Library
add_library(bicycle_node equations_of_motion.h equations_of_motion.cpp target.h target.cpp
        observer.h observer.cpp ${PROTO_SRCS} ${PROTO_HDRS} fixture.cpp fixture.h)
target_link_libraries(bicycle_node PUBLIC ocon::ocon)
target_include_directories(bicycle_node PUBLIC ${Boost_INCLUDE_DIRS} ${CMAKE_CURRENT_BINARY_DIR} ${EIGEN3_INCLUDE_DIR})

# Main application
add_executable(bicycle_main bicycle_main.cpp)
target_link_libraries(bicycle_main PRIVATE bicycle_node ${Boost_LIBRARIES})

# Tester
enable_testing()
add_executable(bicycle_tester bicycle_tester.cpp)
target_link_libraries(bicycle_tester PRIVATE ${GTEST_BOTH_LIBRARIES} pthread bicycle_node)