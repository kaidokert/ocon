/*
 *
 * Copyright 2018 Peter de Lange.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include <gtest/gtest.h>
#include <grpcpp/grpcpp.h>
#include <google/protobuf/util/message_differencer.h>
#include <Eigen/Eigenvalues>

#include <ocon/server.h>

#include "fixture.h"


using namespace bicycle;
using namespace ocon;

class BicycleEOM_Fixture : public BicycleEOM {

public:
    
    // public for testing purposes only
    double time_step = 0.005;
    BicycleEOM_Fixture(): BicycleEOM(nullptr, "bicycle_fixture") {}
    
    void RunSeconds(double seconds) {
        for (unsigned i=0; i < seconds / time_step; i++) {
            ExecuteStep(time_step);
        }
    }

    auto eigenvalues () {
         Eigen::EigenSolver<Eigen::Matrix4d>e(A_, false);
         return e.eigenvalues();
    };


};

TEST(BicycleEOM, Eigenvalues) {

    BicycleEOM_Fixture f;
    f.mutable_inputs()->mutable_initial_state()->set_velocity(5.);

    f.ExecuteInit();
    f.ExecuteStep(0.0);

    EXPECT_NEAR(f.eigenvalues()[0].real(), -14.07838969279822, 1e-6);
    EXPECT_NEAR(f.eigenvalues()[0].imag(),   0.00000000000000, 1e-6);
    EXPECT_NEAR(f.eigenvalues()[1].real(),  -0.77534188219585, 1e-6);
    EXPECT_NEAR(f.eigenvalues()[1].imag(),   4.46486771378823, 1e-6);
    EXPECT_NEAR(f.eigenvalues()[1].real(),  -0.77534188219585, 1e-6);
    EXPECT_NEAR(f.eigenvalues()[2].imag(),  -4.46486771378823, 1e-6);
    EXPECT_NEAR(f.eigenvalues()[3].real(),  -0.32286642900409, 1e-6);
    EXPECT_NEAR(f.eigenvalues()[3].imag(),   0.00000000000000, 1e-6);

}


TEST(BicycleEOM, SteadyState) {
    BicycleEOM_Fixture f;
    f.mutable_inputs()->mutable_initial_state()->set_velocity(5.);
    f.ExecuteInit();
    ASSERT_NEAR(f.outputs().state().velocity(), f.inputs().initial_state().velocity(), 1e-6);

    ASSERT_NEAR(f.outputs().state().roll_angle(), 0., 1e-6);
    ASSERT_NEAR(f.outputs().state().steering_angle(), 0., 1e-6);

    f.RunSeconds(2.);

    ASSERT_NEAR(f.outputs().state().roll_angle(), 0., 1e-6);
    ASSERT_NEAR(f.outputs().state().steering_angle(), 0., 1e-6);
}

TEST(BicycleEOM, Perturbation) {

    BicycleEOM_Fixture f;
    f.mutable_inputs()->mutable_initial_state()->set_velocity(5.);
    f.ExecuteInit();

    // apply a small perturbation and observe
    ASSERT_NEAR(f.outputs().state().velocity(), f.inputs().initial_state().velocity(), 1e-6);
    f.mutable_inputs()->mutable_forces()->set_steer_torque(0.1);
    f.RunSeconds(2.);
    EXPECT_GT(abs(f.outputs().state().roll_angle()), 1e-2);
    EXPECT_GT(abs(f.outputs().state().steering_angle()), 1e-2);

    // remove torque and verify stability
    f.mutable_inputs()->mutable_forces()->set_steer_torque(0.0);
    f.RunSeconds(6.);
    ASSERT_NEAR(f.outputs().state().roll_angle(), 0., 1e-2);
    ASSERT_NEAR(f.outputs().state().steering_angle(), 0., 1e-2);

}

TEST(BicycleFixture, RemoteMethods) {

    // bicycle_fixture model
    BicycleFixture bicycle("bicycle_fixture");

    // server config
    ServerConfiguration config;
    config.set_scheduler(false);
    config.set_port_websocket(8081);
    config.set_port_grpc(50053);
    Server server (&bicycle, config);

    // Go!
    server.Run();

    // client
    auto address = server.config().host() + ":" + std::to_string(server.config().port_grpc());
    auto channel = grpc::CreateChannel(address, grpc::InsecureChannelCredentials());

    auto stub(BicycleService::NewStub(channel));

    std::chrono::system_clock::time_point now = std::chrono::system_clock::now() + std::chrono::milliseconds(1000);
    ASSERT_TRUE(channel->WaitForConnected(now));
    ASSERT_EQ(channel->GetState(true), GRPC_CHANNEL_READY);

    // compare messages
    google::protobuf::util::MessageDifferencer message_differencer;

    // interfaces over grpc
    Empty empty; // sink
    ResetRequest reset_request; // sink
    reset_request.mutable_bicycle_state()->set_velocity(6.);
    reset_request.mutable_bicycle_state()->set_steering_angle(0.05);
    ExternalOutputs outputs;
    ExternalInputs inputs; inputs.mutable_forces()->set_steer_torque(0.1);
    StepRequest step_request;
    step_request.set_time_step(0.01);
    step_request.mutable_external_inputs()->CopyFrom(inputs);

    // reset and update a few times
    grpc::ClientContext ctx_reset;
    EXPECT_TRUE(stub->Reset(&ctx_reset, reset_request, &outputs).ok());
    EXPECT_TRUE(message_differencer.Compare(outputs.bicycle().state(), reset_request.bicycle_state()));
    for (int i = 0; i < 10; i++) {
        grpc::ClientContext ctx_update;
        EXPECT_TRUE(stub->Step(&ctx_update, step_request, &outputs).ok());
        EXPECT_TRUE(message_differencer.Compare(bicycle.external_outputs(), outputs));
    }
    // check get outputs
    grpc::ClientContext ctx_get_outputs;
    EXPECT_TRUE(stub->GetOutputs(&ctx_get_outputs, empty, &outputs).ok());
    EXPECT_TRUE(message_differencer.Compare(bicycle.external_outputs(), outputs));

    // set inputs
    grpc::ClientContext ctx_set_inputs;
    inputs.mutable_forces()->set_pedal_torque(10);
    EXPECT_TRUE(stub->SetInputs(&ctx_set_inputs, inputs, &empty).ok());
    EXPECT_TRUE(message_differencer.Compare(bicycle.external_inputs(), inputs));

}


