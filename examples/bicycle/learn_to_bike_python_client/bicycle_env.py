import grpc
import bicycle_pb2
import bicycle_pb2_grpc
import gym
import numpy as np
from gym import spaces
from gym.utils import seeding

from google.protobuf.empty_pb2 import Empty


class BicycleEnv(gym.Env):

    def __init__(self, host='localhost:50051'):
        self._channel = grpc.insecure_channel(host)
        self._stub = bicycle_pb2_grpc.BicycleServiceStub(self._channel)

        self.time_step = 0.01

        # properties
        self.max_steer_torque = 20
        self.max_pedal_torque = 50
        self.viewer = None

        self.last_error = 0
        self.reward_range = (0, 1)

        # action space
        action_space_high = np.ones(1)
        self.action_space = spaces.Box(low=-action_space_high, high=action_space_high, dtype=np.float32)

        # observation space
        observation_space_high = np.ones(5)
        self.observation_space = spaces.Box(low=-observation_space_high, high=observation_space_high, dtype=np.float)
        self.np_random = None
        self.seed()

    def step(self, u):
        u = np.clip(u, self.action_space.low, self.action_space.high)
        request = bicycle_pb2.StepRequest()
        request.time_step = self.time_step
        request.external_inputs.forces.steer_torque = u[0] * self.max_steer_torque
        output = self._stub.Step(request)
        roll_angle_norm = output.bicycle.state.roll_angle * 4 / np.pi
        observation = self._get_observation(output)
        crash = output.bicycle.crash
        reward = 1 - 0.5 * roll_angle_norm**2 - 0.5 * u[0]**2
        return observation, reward, crash, {}

    def reset(self):
        request = bicycle_pb2.ResetRequest()
        request.bicycle_state.roll_angle = np.random.uniform(-0.1, 0.1)
        request.bicycle_state.roll_rate = np.random.uniform(-0.1, 0.1)
        request.bicycle_state.steering_angle = np.random.uniform(-0.1, 0.1)
        request.bicycle_state.steering_rate = np.random.uniform(-0.1, 0.1)
        request.bicycle_state.velocity = np.random.uniform(3.0, 5.0)
        request.bicycle_state.psi = np.random.uniform(0, 2*np.pi)
        request.target_state.x = np.random.uniform(-40, 40)
        request.target_state.y = np.random.uniform(-40, 40)
        output = self._stub.Reset(request)
        return self._get_observation(output)

    def seed(self, seed_input=None):
        self.np_random, seed = seeding.np_random(seed_input)
        return [seed]

    @staticmethod
    def _get_observation(output):
        return np.array([
            output.bicycle.state.roll_angle / np.pi,
            output.bicycle.state.roll_rate / np.pi,
            output.bicycle.state.steering_angle / np.pi,
            output.bicycle.state.steering_rate / np.pi,
            output.bicycle.state.velocity / 10 - 0.5,
            # output.controller.error_longitudinal / 20,
            # output.controller.error_longitudinal_rate / 20,
            # output.controller.error_lateral / 20,
            # output.controller.error_lateral_rate / 20,
            ])

    def render(self, mode='human'):
        pass

