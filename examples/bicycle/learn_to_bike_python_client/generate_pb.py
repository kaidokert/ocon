import os

proto_files = {
    '..': ['bicycle.proto'],
}

command = 'python -m grpc_tools.protoc -I {source_dir} --python_out={out} --grpc_python_out={out} {source_file}'

for source_dir, source_files in proto_files.items():
    for source_file in source_files:
        os.system(command.format(out='.', source_dir=source_dir, source_file=source_file))


