from keras.models import Model
from keras.layers import Dense, Flatten, Input, concatenate
from keras.optimizers import Adam
from keras.initializers import RandomUniform

from rl.agents import DDPGAgent
from rl.memory import SequentialMemory
from rl.random import OrnsteinUhlenbeckProcess

import numpy as np


def build_actor_model(action_space, observation_space):
    action_input = Input(shape=(1,) + observation_space.shape)
    hidden_layer_size = 32
    observation_size = np.prod(observation_space.shape)
    x = Flatten()(action_input)
    x = Dense(hidden_layer_size, activation="relu",
              kernel_initializer=RandomUniform(-1./np.sqrt(observation_size), 1./np.sqrt(observation_size)),
              bias_initializer=RandomUniform(-1./np.sqrt(observation_size), 1./np.sqrt(observation_size))
              )(x)
    x = Dense(hidden_layer_size, activation="relu",
              kernel_initializer=RandomUniform(-1./np.sqrt(hidden_layer_size), 1./np.sqrt(hidden_layer_size)),
              bias_initializer=RandomUniform(-1./np.sqrt(hidden_layer_size), 1./np.sqrt(hidden_layer_size))
              )(x)
    x = Dense(np.prod(action_space.shape), activation="tanh",
              kernel_initializer=RandomUniform(-3e-3, 3e-3),
              bias_initializer=RandomUniform(-3e-3, 3e-3)
              )(x)
    actor = Model(inputs=action_input, outputs=x)
    return actor


def build_critic_model(action_space, observation_space):
    action_input = Input(shape=(np.prod(action_space.shape),))
    observation_input = Input(shape=(1,) + observation_space.shape)
    observation_size = np.prod(observation_space.shape)
    hidden_layer_size = 32
    second_layer_size = hidden_layer_size + np.prod(action_space.shape)
    x = Flatten()(observation_input)
    x = Dense(hidden_layer_size, activation="relu",
              kernel_initializer=RandomUniform(-1./np.sqrt(observation_size), 1./np.sqrt(observation_size)),
              bias_initializer=RandomUniform(-1./np.sqrt(observation_size), 1./np.sqrt(observation_size))
              )(x)
    x = Dense(hidden_layer_size, activation="relu",
              kernel_initializer=RandomUniform(-1./np.sqrt(second_layer_size), 1./np.sqrt(second_layer_size)),
              bias_initializer=RandomUniform(-1./np.sqrt(second_layer_size), 1./np.sqrt(second_layer_size))
              )(concatenate([x, action_input]))
    x = Dense(1, activation="linear",
              kernel_initializer=RandomUniform(-3e-3, 3e-3),
              bias_initializer=RandomUniform(-3e-3, 3e-3)
              )(x)
    critic = Model(inputs=[action_input, observation_input], outputs=x)
    return critic, action_input


def build_agent(env):
    actor = build_actor_model(env.action_space, env.observation_space)
    critic, critic_action_input = build_critic_model(env.action_space, env.observation_space)
    memory = SequentialMemory(limit=10**6, window_length=1)
    agent = DDPGAgent(
        np.prod(env.action_space.shape),
        actor,
        critic,
        critic_action_input,
        memory,
        random_process=OrnsteinUhlenbeckProcess(theta=0.15, sigma=0.01, dt=0.01),
        train_interval=1, memory_interval=1, batch_size=64, gamma=0.99, target_model_update=.0001,
        nb_steps_warmup_critic=10000, nb_steps_warmup_actor=10000, delta_clip=0.1
    )
    return agent


def run():
    import bicycle_env
    env = bicycle_env.BicycleEnv()
    agent = build_agent(env)
    actor_optimizer = Adam(lr=0.0001)
    critic_optimizer = Adam(lr=0.001)
    agent.compile((actor_optimizer, critic_optimizer), metrics=["mae"])
    # agent.load_weights('result.hdf5')
    agent.fit(env, nb_steps=500000, verbose=2, nb_max_episode_steps=200)
    agent.test(env, nb_episodes=10, nb_max_episode_steps=1000)
    agent.save_weights('result.hdf5', True)


if __name__ == "__main__":
    run()
